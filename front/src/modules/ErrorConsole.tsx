import * as React from 'react'
import styled from 'styled-components'

const StyledDiv = styled.div`
	color: #a94442;
	background-color: #f2dede;
	border-color: #ebccd1;
	padding: 10px;
	text-justify: auto;
	margin-bottom: 10px;
	border: 1px solid #ced4da;
	white-space: pre-wrap;

	.close {
		margin-left: 15px;
		float: right;
		font-size: 21px;
		font-weight: 700;
		line-height: 1;
		color: #000;
		text-shadow: 0 1px 0 #fff;
		filter: alpha(opacity=20);
		opacity: .2;

		&:hover {
			color: #000;
			text-decoration: none;
			cursor: pointer;
			filter: alpha(opacity=50);
			opacity: .5;
		}
	}
}
`

interface Iprops {
	text: string
	close(): void
}

export default class extends React.Component<Iprops> {
	constructor(props: Iprops) {
		super(props);
	}

	render() {
		return (
			<StyledDiv className="text-center error-console" hidden={this.props.text.length === 0}>
				<span className="float-right close" onClick={() => {this.props.close()}}><b>x</b></span>
				<span>{this.props.text}</span>
			</StyledDiv>
		);
	}
}