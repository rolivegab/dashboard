import * as React from 'react';
import styled from 'styled-components';
import MiniLoading from './mini-loading';

interface Iprops {
	text: string;
	isLoading: boolean;
	onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

const StyledButton = styled.button`
	position: relative;
	padding: 0;
	height: 46px;

	& > svg {
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translateX(-50%) translateY(-50%);
	}
`

export default class extends React.Component<Iprops> {
	constructor(props: Iprops) {
		super(props);
	}

	render() {
		if (this.props.isLoading) {
			return (
				<StyledButton className="btn btn-block btn-lg button-loading" onClick={this.props.onClick}>
					<MiniLoading 
						width={40} 
						height={40}
					/>
				</StyledButton>
			);
		} else {
			return (
				<button 
					className="btn btn-block btn-lg button-loading"
				>
					{this.props.text}
				</button>
			);
		}
	}
}