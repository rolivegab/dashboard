import axios from 'axios'
import * as React from 'react'
import { RouteComponentProps } from 'react-router-dom'

export default class<P = any, T = any> extends React.Component<P, T> {
	constructor(props: any) {
		super(props)

		this.state = {} as T
	}

	async request<R>(url: string, data?: any) {
		axios.defaults.withCredentials = true
		const res = await axios.post<R & {maintenanceMode?: boolean, redirect?: string}>(process.env.REACT_APP_SERVER_URL + url, data)
		const props = this.props as any as RouteComponentProps<P>
		if (res.data.maintenanceMode) {
			props.history.push('/')
		} else if (res.data.redirect) {
			props.history.push(res.data.redirect)
		}
		return res.data
	}
}