import Index from 'pages'
import Base from 'pages/base'
import Login from 'pages/login'
import NotFound from 'pages/not-found'
import * as React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

class App extends React.Component {
  public render() {
    return (
      <Switch>
        <Route path="/" component={Index} exact={true} />
        <Route path="/not-found" component={NotFound} exact={true} />
        <Route path="/login" component={Login} />

        {/* Pass control to BASE */}
        <Route path="/home" component={Base} />

        {/* Pass control to POPUP */}
        {/* <Route path="/popup" component={Popup} /> */}

        <Redirect to="/not-found" />
      </Switch>
    );
  }
}

export default App;
