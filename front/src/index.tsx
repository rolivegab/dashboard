import '@fortawesome/fontawesome-free/css/all.css'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { injectGlobal } from 'styled-components'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'

injectGlobal`
  body {
    margin: 0;
    padding: 0;
    font-family: sans-serif;
  }
`

import App from 'App'
import registerServiceWorker from 'registerServiceWorker'

ReactDOM.render(
  <BrowserRouter><App /></BrowserRouter>,
  document.getElementById('root') as HTMLElement
)
registerServiceWorker()
