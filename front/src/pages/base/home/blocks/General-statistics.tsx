import BaseContent from 'client/BaseContent'
import { Loading } from 'modules/Loading';
import * as React from 'react'

interface Istate  {
	loading: boolean
	data: {
		studentcount: number
		coursesLength: number
	}
}

export default class extends BaseContent<{}, Istate> {
	constructor(props: any) {
		super(props)
		this.state = {
			loading: true,
			data: {
				studentcount: 0,
				coursesLength: 0
			},
		}
	}

	componentWillMount() {
		this.request<Istate['data']>('/home', {
			command: 'general-statistics'
		}).then((data) => {
			this.setState({
				...{data},
				loading: false
			})
		})
	}

	render() {
		return this.state.loading
		? (
			<div className="dashboard-block" style={{
				height: '100%',
				display: 'flex',
				justifyContent: 'center',
				alignItems: 'center'
			}}><Loading /></div>
		) : (
			<div className="dashboard-block">
				<h3 className="text-wrap">Estatísticas gerais</h3>
				<table className="table">
					<tbody>
						<tr>
							<td>Total de Alunos</td>
							<td>{this.state.data.studentcount}</td>
						</tr>
						<tr>
							<td>Total de Disciplinas</td>
							<td>{this.state.data.coursesLength}</td>
						</tr>
					</tbody>
				</table>
			</div>
		)
	}
}