import BaseContent from 'client/BaseContent'
import { Loading } from 'modules/Loading'
import * as React from 'react'
import styled from 'styled-components'

const ClickableTD = styled.td`
	cursor: pointer;
`

interface Istate {
	data: {
		mainrolename: string
		courses: Array<{
			id: number
			fullname: string
			selected: boolean
		}>
		usercontextid: number
	}
	loading: boolean
	selectionChanged: boolean
	originalSelection: Istate['data']['courses']
}

export default class extends BaseContent<{}, Istate> {
	constructor(props: any) {
		super(props)

		this.state = {
			data: {
				mainrolename: '',
				courses: [],
				usercontextid: 0,
			},
			loading: true,
			selectionChanged: false,
			originalSelection: []
		}
	}
	componentWillMount() {
		this.request<Istate['data']>('/home', {
			command: 'welcome-user-block'
		}).then((data) => {
			this.setState({
				data: {
					...data,
					courses: data.courses.map((i, indexI) => ({ ...i, selected: indexI === 0 ? false : true }))
				},
				loading: false,
				selectionChanged: true
			})
		})
	}
	toggleCourse(index: number | 'all') {
		this.setState(prevState => ({
			data: {
				...prevState.data,
				courses: index === 'all' 
					? prevState.data.courses.every(i => i.selected)
						? prevState.data.courses.map(i => ({ ...i, selected: false }))
						: prevState.data.courses.map(i => ({ ...i, selected: true }))
					: [
						...prevState.data.courses.slice(0, index),
						{ ...prevState.data.courses[index], selected: !prevState.data.courses[index].selected },
						...prevState.data.courses.slice(index + 1)
					]
			},
			selectionChanged: true,
			originalSelection: prevState.selectionChanged ? prevState.data.courses : prevState.originalSelection
		}))
	}
	applySelection() {

	}
	cancelSelection() {
		this.setState(prevState => ({
			data: {
				...prevState.data,
				courses: prevState.originalSelection
			}
		}))
	}
	render() {
		return this.state.loading
			? (
				<div className="dashboard-block" style={{
					height: '100%',
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center'
				}}><Loading /></div>
			) : (
				<div className="dashboard-block">
					<h3 className="text-wrap">Seja bem-vindo, <b>{this.state.data.mainrolename}</b>!</h3>
					<div className="profile-picture">
						<img src={`http://salavirtual.unifacex.com.br/pluginfile.php/${this.state.data.usercontextid}/user/icon/f3`} alt="Profile picture" />
					</div>
					<h5>Cursos selecionados</h5>
					<table className="table table-sm">
						<tbody>
							<tr>
								<ClickableTD onClick={() => this.toggleCourse('all')}>
									<span
										className={
											this.state.data.courses.every((i) => i.selected)
												? "fa fa-check-square fa-lg"
												: "fa fa-square fa-lg"
										}
									/> Todos
								</ClickableTD>
							</tr>
							{this.state.data.courses.map((i, indexI) => (
								<tr key={indexI}>
									<ClickableTD onClick={() => this.toggleCourse(indexI)}>
										<span
											className={
												i.selected
													? 'fa fa-check-square fa-lg'
													: 'text-muted'
											}
										/> {
											i.selected
												? i.fullname
												: <del> {i.fullname} </del>
										}
									</ClickableTD>
								</tr>
							))}
						</tbody>
					</table>
					<small className="text-wrap">Clique em um curso para selecioná-lo/desselecioná-lo</small>
				</div>
			)
	}
}