import styled from 'styled-components'

const [red, blue, yellow] = ['#CA313E', '#3B9ACA', 	'#CAC531']
const numberOfColumns = 2

export default styled.div`
	background-color: #111C33;
	min-height: 100vh;

	.d-row-space {
		& > * {
			padding: 0px;
			padding-right: 20px;
			padding-bottom: 20px;
		}
	}

	.d-row-complete {
		padding-top: 20px;
		padding-left: 20px;
	}

	.border-red {
		/* border: 5px solid ${red}; */
	}

	.border-blue {
		/* border: 5px solid ${blue}; */
	}

	.border-yellow {
		/* border: 5px solid ${yellow}; */
	}

	.dashboard-block {
		text-align: center;
		padding: 20px;
		color: white;
		background-color: #253D6E;
		overflow-x: auto;
		box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);

		table {
			margin-top: 20px;
			width: 100%;

			tr {
				td {
					width: ${100 / numberOfColumns}%;
					text-align: left;
				}
			}
		}
	}

	.course-selected {
		overflow-y: auto;
	}

	.text-wrap {
		white-space: pre-wrap;
	}

	h3 {
		margin-bottom: 20px;
	}

	.profile-picture {
		width: 100%;
		margin-bottom: 20px;
		img {
			width: 128px;
			height: 128px;
			border-radius: 50%;
			box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
		}
	}
`