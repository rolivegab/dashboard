// import * as Chart from 'chart.js'
import BaseContent from 'client/BaseContent'
// import { Loading } from 'modules/Loading'
import * as React from 'react'
import GeneralStatistics from './blocks/General-statistics'
import Welcome from './blocks/Welcome'
import StyledDiv from './style'
// import { SQL } from 'client/Core'

interface IHomeContentProps {
	updateTime: number
	request: any
}

export default class HomeContent extends BaseContent<IHomeContentProps> {
	constructor(props: IHomeContentProps) {
		super(props);
	}

	showGraphs(aT: any) {
		// let sql = aT.sql as {
		// 	usersbycourse: SQL.SQLData,
		// 	onlineusers: SQL.SQLData
		// };

		// let dataOptions: Chart.ChartDataSets = {
		// 	backgroundColor: '#2C477E',
		// 	hoverBackgroundColor: '#223864'
		// };

		// const usersbycoursedata = SQL.toChartData(sql.usersbycourse, 'Curso', 'name', 'enrolledusers', dataOptions);
		// const onlineusers = SQL.toChartData(sql.onlineusers, 'bebebe', 'name', 'onlineusers', dataOptions);

		// if (!usersbycoursedata || !onlineusers) {
		// 	return;
		// }

		// let options = (): Chart.ChartOptions => { 
		// 	return {
		// 		maintainAspectRatio: false,
		// 		responsive: true,
		// 		scales: {
		// 			yAxes: [{
		// 				type: 'logarithmic',
		// 				ticks: {
		// 					min: 1,
		// 					max: SQL.maxFunc(usersbycoursedata),
		// 					callback: function(value: string) {
		// 						return Number(value);
		// 					}
		// 				},
		// 				gridLines: {
		// 					display: true,
		// 					color: 'rgba(a1,a1,a1,0.2)'
		// 				},
		// 				afterBuildTicks: function (
		// 					chart: {
		// 						ticks: number[]
		// 					}
		// 				) {
		// 					chart.ticks = [];
		// 					chart.ticks.push(1);
		// 					chart.ticks.push(5);
		// 					chart.ticks.push(25);
		// 					chart.ticks.push(125);
		// 					chart.ticks.push(625);
		// 				}
		// 			}],
		// 			xAxes: [{
		// 				gridLines: {
		// 					display: false
		// 				},
		// 				ticks: {
		// 					autoSkip: false
		// 				}
		// 			}]
		// 		},
		// 		plugins: {
		// 			// showDataChart: Scripts.class('showDataChart').plugin()
		// 		}
		// 	};
		// };

		// console.log(options);
	}

	render() {
		return (
			<StyledDiv>
				<div className="container-fluid">
					<div className="row d-row-complete">
						<div className="col-xl-3 col-lg-6 col-md-6 p-0">
							<div className="container-fluid">
								<div className="row d-row-space">
									<div className="col-12">
										<Welcome />
									</div>
									<div className="col-12">
										<div className="dashboard-block">
											oi
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="col-xl-6 col-lg-6 col-md-6 p-0">
							<div className="container-fluid">
								<div className="row d-row-space">
									<div className="col-12">
										<GeneralStatistics />
									</div>
									<div className="col-12">
										<div className="dashboard-block">
											oi
											</div>
									</div>
								</div>
							</div>
						</div>
						<div className="col-xl-3 col-lg-6 col-md-6 p-0">
							<div className="container-fluid">
								<div className="row d-row-space">
									<div className="col-12">
										<div className="dashboard-block">
											oi
											</div>
									</div>
									<div className="col-12">
										<div className="dashboard-block">
											oi
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</StyledDiv>
		)
	}
}
/*<div className="profile-picture">
										<img src={`http://salavirtual.unifacex.com.br/pluginfile.php/${this.state.usercontextid}/user/icon/f3`} alt="Profile picture" />
									</div>
									<h3 className="text-wrap">Seja bem-vindo, <b>{this.state.mainrolename}</b>!</h3>
									<table className="table table-sm">
										<tbody>
											<tr>
												<td>Total de Alunos</td>
												<td>{this.state.studentcount}</td>
											</tr>
											<tr>
												<td>Total de Disciplinas</td>
												<td>{this.state.courses.length}</td>
											</tr>
										</tbody>
									</table>
									<h3>Cursos selecionados</h3>
									<table className="table">
										<tbody>
											{this.state.courses.map((i, indexI) => (
												<tr><td data-id={i.id}>{i.fullname}</td></tr>
											))}
										</tbody>
									</table>*/
// import { App } from 'client/App';
// import { AJAX, SQL } from 'client/Core';
// import { Chart, ChartDataSets } from 'chart.js';

// interface State {
// 	studentcount: string;
// 	teachercount: string;
// 	coursecount: string;
// }

// class Content extends React.Component<Props, State> {

// 	timerID: NodeJS.Timer;
// 	topdivchart?: Chart;
// 	bottomdivchart?: Chart;

// 	constructor(props: Props) {
// 		super(props);
// 		this.state = {
// 			studentcount: '-',
// 			teachercount: '-',
// 			coursecount: '-'
// 		};
// 	}

// 	componentDidMount() {
// 		this.updateInfo();
// 	}

// 	componentWillUnmount() {
// 		clearInterval(this.timerID);
// 	}

// 	updateInfo() {
// 		let aux = () => {
// 			let header = {
// 				ajax: 'HomeContent',
// 				command: 'sql'
// 			};

// 			App.request(header, (aT: AJAX.ArrayText) => {
// 				this.setState({
// 					studentcount: aT.studentcount as string,
// 					teachercount: aT.teachercount as string,
// 					coursecount: aT.coursecount as string
// 				});

// 				this.showGraphs(aT);
// 			});
// 		};

// 		this.timerID = setInterval(
// 			aux, 
// 			App.updateTime
// 		);

// 		aux();
// 	}

// 	showGraphs(aT: AJAX.ArrayText) {
// 		let sql = aT.sql as {
// 			usersbycourse: SQL.SQLData,
// 			onlineusers: SQL.SQLData
// 		};

// 		let dataOptions: ChartDataSets = {
// 			backgroundColor: '#2C477E',
// 			hoverBackgroundColor: '#223864'
// 		};

// 		const usersbycoursedata = SQL.toChartData(sql.usersbycourse, 'Curso', 'name', 'enrolledusers', dataOptions);
// 		const onlineusers = SQL.toChartData(sql.onlineusers, 'bebebe', 'name', 'onlineusers', dataOptions);

// 		if (!usersbycoursedata || !onlineusers) {
// 			return;
// 		}

// 		let options = (): Chart.ChartOptions => { 
// 			return {
// 				maintainAspectRatio: false,
// 				responsive: true,
// 				scales: {
// 					yAxes: [{
// 						type: 'logarithmic',
// 						ticks: {
// 							min: 1,
// 							max: SQL.maxFunc(usersbycoursedata),
// 							callback: function(value: string) {
// 								return Number(value);
// 							}
// 						},
// 						gridLines: {
// 							display: true,
// 							color: 'rgba(a1,a1,a1,0.2)'
// 						},
// 						afterBuildTicks: function (
// 							chart: {
// 								ticks: number[]
// 							}
// 						) {
// 							chart.ticks = [];
// 							chart.ticks.push(1);
// 							chart.ticks.push(5);
// 							chart.ticks.push(25);
// 							chart.ticks.push(125);
// 							chart.ticks.push(625);
// 						}
// 					}],
// 					xAxes: [{
// 						gridLines: {
// 							display: false
// 						},
// 						ticks: {
// 							autoSkip: false
// 						}
// 					}]
// 				},
// 				plugins: {
// 					// showDataChart: Scripts.class('showDataChart').plugin()
// 				}
// 			};
// 		};

// 		let options1, options2;

// 		options1 = options();
// 		options2 = options();

// 		options1.title = {
// 			display: true,
// 			text: 'Usuários por Categoria'
// 		};

// 		options2.title = {
// 			display: true,
// 			text: 'Usuários online por Categoria'
// 		};

// 		if (!this.topdivchart) {
// 			this.topdivchart = new Chart('topdiv', {
// 				type: 'bar',
// 				options: options1,
// 				data: usersbycoursedata
// 			});
// 		}

// 		if (!this.bottomdivchart) {
// 			this.bottomdivchart = new Chart('bottomdiv', {
// 				type: 'bar',
// 				options: options2,
// 				data: onlineusers
// 			});
// 		}

// 		if (Chart || usersbycoursedata || onlineusers) {
// 			// return;
// 		}
// 	}

// 	render() {
// 		let counter = 0;
// 		return ([
// 			(
// 				<div key={counter++} id="firstcontent">
// 					<div className="showButton red rightspace">
// 						<img className="inlineblock icon" src="/pages/HomeContent/image/alunos.png" alt="Student Icon"/>
// 						<div className="inlineblock leftspace"> 
// 							<p>Total alunos</p>
// 							<div id="studentcount" className="number">
// 								{this.state.studentcount}
// 							</div>
// 						</div>
// 					</div>
// 					<div className="showButton blue">
// 						<img className="inlineblock icon" src="/pages/HomeContent/image/professores.png" alt="Teacher Icon"/>
// 						<div className="inlineblock leftspace"> 
// 							<p>Total professores</p>
// 							<div id="teachercount" className="number">
// 								{this.state.teachercount}
// 							</div>
// 						</div>
// 					</div>
// 					<div className="showButton yellow leftspace">
// 					<img className="inlineblock icon" src="/pages/HomeContent/image/cursos.png" alt="Course Icon"/>
// 						<div className="inlineblock leftspace">
// 							<p>Total cursos / disciplinas</p>
// 							<div id="coursecount" className="number">
// 								{this.state.coursecount}
// 							</div>
// 						</div>
// 					</div>
// 				</div>
// 			),

// 			(
// 				<div key={counter++} className="chart-wrapper">
// 					<canvas id="topdiv"/>
// 				</div>
// 			),

// 			(
// 				<div key={counter++} className="chart-wrapper">
// 					<canvas id="bottomdiv"/>
// 				</div>
// 			)
// 		]);
// 	}
// }

// export default class HomeContent extends Page {
// 	constructor() {
// 		let basecontent = document.getElementById('basecontent') as HTMLElement;
// 		let styles = ['homecontent'];
// 		let resources: RawResources = {
// 			css: [	
// 				'body'
// 			]
// 		};
// 		let scripts: RawScripts = {
// 			0: [
// 				'drawCharts'
// 			]
// 		};

// 		super(HomeContent.name, basecontent, resources, styles, scripts);
// 		this.setContent(<Content scripts={this.scripts}/>);
// 	}
// }