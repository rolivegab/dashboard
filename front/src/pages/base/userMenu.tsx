import BaseContent from 'client/BaseContent'
import * as React from 'react'

export enum UserMenuFunctions {
	logout = 'logout'
}

interface IUserMenuState {
	isOpen: boolean;
	firstname: string;
	lastname: string;
	userid?: number;
	items: Array<{
		name: string;
		action: string;
	}>;
}

interface Iprops {
	onClick(commandName: string): void
}

export default class extends BaseContent<Iprops, IUserMenuState> {
	constructor(props: any) {
		super(props);
		this.state = {
			isOpen: false,
			firstname: '-',
			lastname: '-',
			items: []
		};

		// Bindings:
		this.divUsernameHandleClick = this.divUsernameHandleClick.bind(this);
	}

	componentDidMount() {
		this.request('/user-menu', {
			command: 'usermenu'
		}).then((arrayText: any) => {
			this.setState({
				firstname: arrayText.firstname,
				lastname: arrayText.lastname,
				items: arrayText.items
			});
		});
	}

	divUsernameHandleClick() {
		this.setState(prevState => ({
			isOpen: !prevState.isOpen
		}));
	}

	renderMenuItems(): JSX.Element[] {
		return this.state.items.map((i, indexI) => {
			return (
				<div key={indexI} className="itemMenu" onClick={() => this.props.onClick(i.action)}>
					{i.name}
				</div>
			);
		});
	}

	render() {
		return (
			<div id="divUserName" onClick={this.divUsernameHandleClick}>
				<span id="userName">
					{this.state.firstname + ' ' + this.state.lastname}
				</span>
				<span key={0} id="caretDown" className="fa fa-caret-down" />
				<div key={1} id="dropMenu" hidden={!this.state.isOpen}>
					{this.renderMenuItems()}
				</div>
			</div>
		);
	}
}