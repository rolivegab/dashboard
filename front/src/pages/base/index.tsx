import BaseContent from 'client/BaseContent'
import { Loading } from 'modules/Loading';
import * as React from 'react'
import { Route, RouteComponentProps, Switch, withRouter } from 'react-router'
import Home from './home'
import SideNav, { Igroup } from './sideNav'
import StyledDiv from './style'
import UserMenu from './userMenu'

interface Istate {
	loading: boolean
	data: {
		sideNavItems: SideNav['props']['items']
	}
	isSidenavOpen: boolean
}

const RouterSideNav = withRouter(SideNav)
const RouterUserMenu = withRouter(UserMenu)

export default class extends BaseContent<RouteComponentProps<{}>, Istate> {
	userMenuCommands: {
		logout(): void
	}
	constructor(props: any) {
		super(props)

		this.state = {
			data: {
				sideNavItems: [],
			},
			loading: true,
			isSidenavOpen: false,
		}

		this.userMenuCommands = {
			logout: () => {
				this.request<{ success: true }>('/', {
					command: 'logout',
				}).then((data) => {
					if (data.success === true) {
						this.props.history.push('/login')
					}
				})
			}
		}
	}

	componentDidMount() {
		this.request<Istate['data']>('/', {
			command: 'load-base-data',
		}).then((data) => {
			this.setState(prevState => ({
				...prevState,
				loading: false,
				data: {
					sideNavItems: data.sideNavItems
				}
			}))
		})
	}

	toggleSideNav = (e: React.MouseEvent<HTMLDivElement>) => {
		this.setState(prevState => ({
			isSidenavOpen: !prevState.isSidenavOpen
		}))
	}

	toggleGroupOpen = (groupIndex: number) => {
		this.setState(prevState => ({
			...prevState,
			data: {
				sideNavItems: [
					...prevState.data.sideNavItems.slice(0, groupIndex),
					{ ...prevState.data.sideNavItems[groupIndex], isOpen: !(prevState.data.sideNavItems[groupIndex] as Igroup).isOpen },
					...prevState.data.sideNavItems.slice(groupIndex + 1)
				]
			}
		}))
	}

	userMenuCallCommand = (commandName: string) => {
		if (this.userMenuCommands && commandName in this.userMenuCommands) {
			this.userMenuCommands[commandName]()
		}
	}

	render() {
		return this.state.loading
			? (
				<div style={{
					display: 'flex',
					justifyContent: 'center',
					alignItems: 'center',
					height: '100%'
				}}>
					<Loading />
				</div>
			)
			: (
				<StyledDiv>
					<RouterSideNav
						isOpen={this.state.isSidenavOpen}
						items={this.state.data.sideNavItems}
						toggleGroupOpen={this.toggleGroupOpen}
						openPage={() => null}
					/>
					<div id="group" className={this.state.isSidenavOpen ? undefined : 'closed'}>
						<div id="header-top">
							<div id="sidenav-button" onClick={this.toggleSideNav}>
								&#9776;
							</div>
							<img id="logo" src="/image/logo_ead_dashboard.png" alt="Dashboard LOGO" />
							<div id="general-status">
								<div className="info">
									<span> Server: </span>
									<span id="server-status" className="font-awesome" />
								</div>
								<div className="info">
									<span> Database: </span>
									<span id="database-status" className="font-awesome" />
								</div>
							</div>
							<RouterUserMenu onClick={this.userMenuCallCommand} />
						</div>
						<div id="base" className={this.state.isSidenavOpen ? '' : 'closed'}>
							<div id="basecontent">
								<Switch>
									<Route path={'/home'} component={Home} />
									<Route path={'/estatisticas/taxa-evasao'} render={() => (<div> oi </div>)} />
									<Route path={'/estatisticas/modulos'} />
									<Route path={'/estatisticas/atividades'} />
								</Switch>
							</div>
						</div>
					</div>
				</StyledDiv>
			)
	}
}