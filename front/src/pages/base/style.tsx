import styled from "styled-components";

export default styled.div`
	white-space: nowrap;
	background-color: #060b11;
	#group {
		font-size: 16px;
		display: inline-block;
		vertical-align: top;
		width: 85%;
		transition: width 0.5s;
		overflow: hidden;
		height: 100%;

		/* Ativa sempre que a tela for no máximo 866px */
		@media (max-width: 866px) {
			width: calc(100% - 130px);
		}

		&.closed {
			width: 100%;
		}
	}

	.row-content {
		white-space: nowrap;
		height: 100%;
	}

	#basecontent {
		overflow: hidden;
	}
	
	#header-top {
		display: flex;
		flex-direction: row;
		width: 100%;
		background-color: #060A12;
		flex-wrap: wrap;
		padding: 5px;
		overflow-x: auto;
		box-sizing: border-box;
		min-height: 60px;

		> * {
			align-self: center;
		}

		font-size: 14px;

		#logo {
			font-size: initial;
			height: 2.5em;
			display: block;
		}

		#sidenav-button {
			font-size: 24px;
			color: white;
			padding-left: 10px;
			padding-right: 10px;
			cursor: pointer; cursor: hand;
			transition: font-size 0.2s cubic-bezier(0, 0.66, 0.83, 1);

			&:hover {
				font-size: 30px;
			}
		}

		#general-status {
			padding-left: 10px;

			.info {
				display: flex;
				justify-content: space-between;
				white-space: nowrap;
			}

			span
			{
				&:first-child {
					color: white;
				}

				&:last-child {
					padding-left: 15px;
					font-weight: bold;

					&.online {
						color: #31CA64;
					}

					&.offline {
						color: #CA313E;
					}
				}
			}
		}

		#divUserName {
			display: inline-block;
			cursor: pointer;
			user-select: none;
			-webkit-user-select: none;
			flex-grow: 1;
			text-align: right;

			#userName {
				font-size: 14px;
				color: white;
				text-decoration: none;
				margin-right: 20px;
			}

			#caretDown {
				font-size: 14px;
				color: white;
				text-decoration: none;
				margin-right: 20px;
			}
		}

		#dropMenu {
			z-index: 1;
			position: absolute;
			right: 20px;
			top: 45px;
			width: 130px;
			background-color: white;
			border: 1px solid;
			border-color: #e8e3e3;
			box-shadow: 0 5px 10px rgba(0,0,0,0.2);

			::after {
				content: '';
				display: inline-block;
				border-left: 6px solid transparent;
				border-right: 6px solid transparent;
				border-bottom: 6px solid #fff;
				position: absolute;
				right: 20px;
				top: -6px;
			}

			.itemMenu {
				color: black;
				text-align: center;
				cursor: pointer;
				padding: 10px;

				&:hover {
					background-color: #eee;
				}
			}
		}
	}
`;