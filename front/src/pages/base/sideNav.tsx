import BaseContent from 'client/BaseContent'
import * as React from 'react'
import { Link, RouteComponentProps } from 'react-router-dom'
import StyledNav from './sideNav.style'

export interface Iitem {
	text: string
	url: string
}

export interface Igroup {
	text: string
	isOpen: boolean
	children: Iitem[]
}

interface Iprops {
	isOpen: boolean,
	items: Array<Igroup | Iitem>
	toggleGroupOpen: (index: number) => void
	openPage: (url: string) => void
}

export default class extends BaseContent<Iprops & RouteComponentProps<{}>> {
	constructor(props: any) {
		super(props)
	}

	renderItem(item: Iitem, index: number) {
		return (
			<Link
				key={index}
				className="dropbtn"
				to={item.url}
			>
				{item.text}
			</Link>
		)
	}

	renderGroup(group: Igroup, index: number) {
		return (
			<div key={index}>
				<div className="plushint">
					<div className="plushint-space" />
					<div className="plushint-symbol"><span className={`fa ${group.isOpen ? ' fa-caret-up' : ' fa-caret-down'}`}/></div>
				</div>
				<div
					className="dropbtn"
					onClick={() => this.props.toggleGroupOpen(index)}
				>
					{group.text}
				</div>
				<div
					key={2}
					className="dropdown-content"
					style={{
						maxHeight: group.isOpen ? 41 * group.children.length + 'px' : '0px'
					}}
				>
					{group.children.map((j, indexJ) => (
						this.renderItem(j, indexJ)
					))}
				</div>
			</div>
		)
	}

	renderItems() {
		return (
			this.props.items.map((i, indexI) => {
				if (i.hasOwnProperty('url')) {
					const item = i as Iitem
					return this.renderItem(item, indexI)
				} else if (i.hasOwnProperty('children')) {
					const group = i as Igroup
					return this.renderGroup(group, indexI)
				} else {
					return null
				}
			})
		);
	}

	render() {
		return (
			<StyledNav
				className={(this.props.isOpen ? undefined : 'closed')}
			>
				{this.renderItems()}
			</StyledNav>
		)
	}
}