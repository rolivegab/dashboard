import styled from 'styled-components'

export default styled.nav`
	vertical-align: top;
	display: inline-block;
	text-align: center;
	width: 15%;
	min-width: 130px;
	font-size: 14px;
	transition: margin-left 0.5s;

	a {
		display: block;
		text-decoration: none;
	}

	&.closed {
		margin-left: -15%;
	}

	@media (max-width: 866px)
	{
		&.closed {
			margin-left: -130px;
		}
	}

	#sidenav-button {
		user-select: none;
		-webkit-user-select: none;
	}	

	/* Botão */
	.dropbtn {
		box-sizing: border-box;
		width: 100%;
		min-width: 130px;
		color: white;
		padding: 10px;
		cursor: pointer;
		white-space: normal;

		&.active {
			background: #223864;
		}

		&:hover {
			background-color: #111d34;
		}
	}

	.dropdown-content {
		overflow: hidden;
		transition: all 0.5s ease-in-out;
		max-height: 0px;
	}

	.plushint {
		position: absolute;
		font-size: 14px;
		font-family: 'Open Sans', sans serif;
		color: white;
		width: 15%;
		height: 35px;
		min-width: 130px;
		pointer-events: none;
		transition: margin-left 0.5s;	
	}

	.plushint-symbol {
		display: table-cell;
			vertical-align: middle;
			height: 35px;
			padding-right: 10px;
	}

	.plushint-space {
		display: table-cell;
		width: 100%;
	}
`