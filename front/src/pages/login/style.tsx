import styled from 'styled-components'

export default styled.div`
	width: 100%;
	height: 100%;
	margin: auto;
	background-size: 50px;
	background-color: gray;

	#center-div {
		font-size: 14px;
		border-radius: 4px;
		box-sizing: border-box;
		position: absolute;
		top: 50%;
		left: 50%;
		width: 300px;
		margin: -150px 0 0 -150px;
		background-color: white;
		padding: 20px; }

	.btn {
		display: table;
		box-sizing: border-box;
		padding: 10px;
		font-size: 14px;
		border: 1px solid rgba(0, 0, 0, 0.3);
		border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
		border-radius: 4px;
		box-shadow: inset 0 -5px 45px rgba(100, 100, 100, 0.2), 0 1px 1px rgba(255, 255, 255, 0.2);
		-webkit-transition: box-shadow .5s ease;
		-moz-transition: box-shadow .5s ease;
		-o-transition: box-shadow .5s ease;
		-ms-transition: box-shadow .5s ease;
		transition: box-shadow .5s ease; }

	.btn:hover, .btn:active, .btn.active, .btn.disabled, .btn[disabled] {
		background-color: #e6e6e6; }

	.input {
		margin: auto;
		margin-bottom: 10px;
		display: table;
		box-sizing: border-box;
		width: 100%;
		padding: 10px;
		font-size: 13px;
		border: 1px solid rgba(0, 0, 0, 0.3);
		border-radius: 4px;
		box-shadow: inset 0 -5px 45px rgba(100, 100, 100, 0.2), 0 1px 1px rgba(255, 255, 255, 0.2);
		-webkit-transition: box-shadow .5s ease;
		-moz-transition: box-shadow .5s ease;
		-o-transition: box-shadow .5s ease;
		-ms-transition: box-shadow .5s ease;
		transition: box-shadow .5s ease; }

	input:focus {
		box-shadow: inset 0 -5px 45px rgba(100, 100, 100, 0.4), 0 1px 1px rgba(255, 255, 255, 0.2); }

	#logov2 {
		box-sizing: border-box;
		margin-bottom: 10px;
		width: 100%; }

	body {
		background-color: gray; }
`