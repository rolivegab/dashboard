import BaseComponent from 'client/BaseContent'
import ErrorConsole from 'modules/ErrorConsole'
import LoadingButton from 'modules/LoadingButton'
import * as React from 'react'
import { RouteComponentProps } from 'react-router';
import StyledDiv from './style'

interface Istate {
	username: string
	password: string
	errorMsg: string
	submiting: boolean
}

export default class extends BaseComponent<RouteComponentProps<{}>, Istate> {
	constructor(props: any) {
		super(props)

		this.state = {
			username: '',
			password: '',
			errorMsg: '',
			submiting: false
		}
	}

	username = (e: React.ChangeEvent<HTMLInputElement>) => {
		const value = e.currentTarget.value
		this.setState({
			username: value
		})
	}

	password = (e: React.ChangeEvent<HTMLInputElement>) => {
		const value = e.currentTarget.value
		this.setState({
			password: value
		})
	}

	onSubmit = () => {
		const [username, password] = [this.state.username, this.state.password]
		this.setState({
			submiting: true,
		})

		this.request<{ success: boolean, mainroleid: number }>('/login', {
			...{ username },
			...{ password },
		}).then((data) => {
			this.setState({
				submiting: false,
			})
			if (data.success === true) {
				if ([2, 3].includes(data.mainroleid)) {
					this.props.history.push('/home')
				} else {
					this.setState({
						errorMsg: 'Acesso proibido para estudantes',
					})
				}
			} else {
				this.setState({
					errorMsg: 'Usuário/Senha incorretos!',
				})
			}
		})
	}

	render() {
		return (
			<StyledDiv>
				<div id="background-div">
					<div id="center-div">
						<img id="logov2" src="/image/logo_ead_dashboard.png" alt="Imagem representando a logo da Dashboard." />
						<ErrorConsole text={this.state.errorMsg} close={() => {
							this.setState({
								errorMsg: ''
							})
						}} />
						<form onSubmit={this.onSubmit} action="javascript:void(0)">
							Usuário: <br />
							<input
								className="input text"
								type="text"
								name="username"
								required={true}
								onChange={this.username}
								value={this.state.username}
							/>
							Senha: <br />
							<input
								className="input password"
								type="password"
								name="password"
								required={true}
								onChange={this.password}
								value={this.state.password}
							/>
							<br />
							<LoadingButton isLoading={this.state.submiting} text="Entrar" />
							{/* <button className="btn">Entrar</button> */}
						</form>
					</div>
				</div>
			</StyledDiv>
		)
	}
}