import BaseContent from 'client/BaseContent'
import Loading from 'modules/Loading'
import * as React from 'react'
import { Redirect, RouteComponentProps } from 'react-router'
import styled from 'styled-components'

const StyledDiv = styled.div`
	height: 100%;
	width: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
`

interface Istate {
	loading: boolean
	logged?: boolean
}

export default class extends BaseContent<RouteComponentProps<{}>, Istate> {
	constructor(props: any) {
		super(props)

		this.state = {
			loading: true
		}
	}

	componentWillMount() {
		this.request<{ logged: boolean }>('/', {
			command: 'logged'
		}).then((data) => {
			if (typeof data.logged === 'boolean') {
				this.setState({
					loading: false,
					logged: data.logged
				})
			}
		})
	}

	render() {
		return (
			<StyledDiv className="container-fluid">
				{this.state.loading
					? <Loading />
					: (
						this.state.logged && this.state.logged
							? <Redirect to="/home" />
							: <Redirect to="/login" />
					)
				}
			</StyledDiv>
		)
	}
}