interface Sess {
	user?: {
		username: string
		userid: number
		mainrolename: string
		mainroleid: number
		courses: Array<{
			id: number
			name: string
		}>
	}
}

type SelectConnQuery<T> = Array<Array<T>>