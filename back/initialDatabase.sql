CREATE TABLE IF NOT EXISTS server (
	is_in_maintenance BOOLEAN NOT NULL,
	maintenance_date INTEGER,
	is_user_registration_open BOOLEAN NOT NULL
);

INSERT INTO server VALUES (false, NULL, false);

CREATE TABLE IF NOT EXISTS user (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	cpf VARCHAR(11) UNIQUE NOT NULL,
	email VARCHAR(255) UNIQUE NOT NULL,
	time_created INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS user_info (
	user_id INTEGER PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	rg VARCHAR(15) NOT NULL,
	birthday INTEGER NOT NULL,
	address VARCHAR(255) NOT NULL,
	number VARCHAR(20) NOT NULL,
	cep VARCHAR(8) NOT NULL,
	city VARCHAR(50) NOT NULL,
	tel VARCHAR(11) NOT NULL,
	FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE IF NOT EXISTS user_email_confirmation (
	user_id INTEGER PRIMARY KEY,
	token VARCHAR(32) NOT NULL,
	validated BOOLEAN NOT NULL,
	time_created INTEGER NOT NULL,
	FOREIGN KEY (user_id) REFERENCES user(id)
);

CREATE TABLE IF NOT EXISTS role (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(20) NOT NULL
);

INSERT INTO role VALUES (default, 'conteudist');
INSERT INTO role VALUES (default, 'reviewer');
INSERT INTO role VALUES (default, 'manager');
INSERT INTO role VALUES (default, 'director');

CREATE TABLE IF NOT EXISTS file (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	basename VARCHAR(150),
	extension VARCHAR(10),
	token VARCHAR(32)
);

CREATE TABLE IF NOT EXISTS graduation_inscription (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	user_id INTEGER,
	best_degree VARCHAR(100) NOT NULL,
	graduation_for VARCHAR(150) NOT NULL,
	pos_graduation_for VARCHAR(150),
	employed_before BOOLEAN NOT NULL,
	teacher_experience VARCHAR(1000) NOT NULL,
	ead_experience VARCHAR(1000) NOT NULL,
	content_manager_experience VARCHAR(1000) NOT NULL,
	other_experiences VARCHAR(1000) NOT NULL,
	lattes_file_id INTEGER NOT NULL,
	rg_file_id INTEGER NOT NULL,
	cpf_file_id INTEGER NOT NULL,
	elector_file_id INTEGER NOT NULL,
	residence_file_id INTEGER NOT NULL,
	graduation_file_front_id INTEGER NOT NULL,
	graduation_file_verse_id INTEGER,
	pos_graduation_file_front_id INTEGER,
	pos_graduation_file_verse_id INTEGER,
	FOREIGN KEY (lattes_file_id) REFERENCES file(id),
	FOREIGN KEY (rg_file_id) REFERENCES file(id),
	FOREIGN KEY (cpf_file_id) REFERENCES file(id),
	FOREIGN KEY (elector_file_id) REFERENCES file(id),
	FOREIGN KEY (residence_file_id) REFERENCES file(id),
	FOREIGN KEY (graduation_file_front_id) REFERENCES file(id),
	FOREIGN KEY (graduation_file_verse_id) REFERENCES file(id),
	FOREIGN KEY (pos_graduation_file_front_id) REFERENCES file(id),
	FOREIGN KEY (pos_graduation_file_verse_id) REFERENCES file(id)
);

CREATE TABLE IF NOT EXISTS graduation_inscription_role (
	graduation_inscription_id INTEGER,
	role_id INTEGER,
	PRIMARY KEY (graduation_inscription_id, role_id),
	FOREIGN KEY (graduation_inscription_id) REFERENCES graduation_inscription(id),
	FOREIGN KEY (role_id) REFERENCES role(id)
);

CREATE TABLE IF NOT EXISTS graduation_area (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(200) NOT NULL
);

CREATE TABLE IF NOT EXISTS graduation_course (
	id INTEGER PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(200) NOT NULL
);

CREATE TABLE graduation_inscription_course (
	graduation_course_id INTEGER,
	graduation_inscription_id INTEGER,
	PRIMARY KEY (graduation_course_id, graduation_inscription_id),
	FOREIGN KEY (graduation_course_id) REFERENCES graduation_course(id),
	FOREIGN KEY (graduation_inscription_id) REFERENCES graduation_inscription(id)
);

CREATE TABLE IF NOT EXISTS user_role (
	user_id INTEGER,
	role_id INTEGER,
	PRIMARY KEY (user_id, role_id),
	FOREIGN KEY (user_id) REFERENCES user(id),
	FOREIGN KEY (role_id) REFERENCES role(id)
);

CREATE TABLE IF NOT EXISTS graduation_area_course (
	graduation_area_id INTEGER,
	graduation_course_id INTEGER,
	PRIMARY KEY(graduation_course_id, graduation_area_id),
	FOREIGN KEY (graduation_area_id) REFERENCES graduation_area(id),
	FOREIGN KEY (graduation_course_id) REFERENCES graduation_course(id)
);

CREATE TABLE IF NOT EXISTS graduation_inscription_file (
	graduation_inscription_id INTEGER,
	file_id INTEGER,
	PRIMARY KEY (graduation_inscription_id, file_id),
	FOREIGN KEY (graduation_inscription_id) REFERENCES graduation_inscription(id),
	FOREIGN KEY (file_id) REFERENCES file(id)
);