import Express from 'express'

export default Express.Router().use(async (req, res, next) => {
	if (!req.session || !req.session.user) {
		res.json({
			redirect: '/login',
		})
	} else {
		next()
	}
})
