import Express from 'express'

export default Express.Router().use(async (req, res, next) => {
	if (req.session) {
		const S = req.session as Sess
		if (S.user && S.user.mainroleid === 3) {
			next()
		} else {
			req.session.destroy(() => {
				res.json({
					redirect: '/login',
				})
			})
		}
	} else {
		res.json({
			redirect: '/login',
		})
	}
})
