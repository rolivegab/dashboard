import Express from 'express'
import RequireLogin from '../../middlewares/require-login'

const Router = Express.Router()

Router
	.use(RequireLogin)
	.post('/', (req, res) => {
		if (req.body.command === 'usermenu') {
			// Getting firstname and lastname
			res.json({
				firstname: 'Gabriel',
				items: [{
					action: 'logout',
					name: 'Sair',
				}],
				lastname: 'Rocha de Oliveira	',
			})
		} else {
			res.json({
				success: false,
			})
		}
	})

export default Router
