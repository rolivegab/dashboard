import BodyParser from 'body-parser'
import Express from 'express'
import Base from './base/router'
import Login from './login/router'
import UserMenu from './user-menu/router'

const Route = Express.Router()

Route
	// Processa parâmetros POST do tipo JSON recebidos pela requisição do cliente para poderem ser lidos através do req.body
	.use(BodyParser.json())

	// Executa o router 'Base' no caminho '/'
	.use('/', Base)

	// Executa o router 'Login' no caminho '/login'
	.use('/login', Login)

	// Executa o router 'UserMenu' no caminho '/user-menu'
	.use('/user-menu', UserMenu)

export default Route
