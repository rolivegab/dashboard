import Express from 'express'
import loadUserInfo from '../../lib/load-user-info'
import Home from './home/router'

const Route = Express.Router();

Route
	.post('/', async (req, res) => {
		if (!req.session) {
			return
		}
		if (req.body.command === 'logged') {
			if (req.session.username) {
				res.json({
					redirect: '/select-course',
				})
			} else {
				res.json({
					redirect: '/login',
				})
			}
		} else if (req.body.command === 'logout') {
			if (req.session) {
				req.session.destroy(() => {
					res.json({
						success: true,
					})
				})
			} else {
				res.json({
					success: true,
				})
			}
		} else if (req.body.command === 'load-base-data') {
			res.json({
				sideNavItems: [
					{
						text: 'Home',
						url: '/home',
					},
					{
						children: [
							{
								text: 'Taxa de Evasão',
								url: '/estatisticas/taxa-evasao',
							},
							{
								text: 'Módulos',
								url: '/estatisticas/modulos',
							},
							{
								text: 'Atividade',
								url: '/estatisticas/atividades',
							},
						],
						isOpen: false,
						text: 'Estatísticas',
					},
				],
			})
		} else if (req.body.command === 'reload-user-data') {
			const S = req.session as Sess
			if (S.user) {
				await loadUserInfo(S.user.userid, req)
				res.json({
					success: true,
				})
			}
		}
	})
	.use('/home', Home)

export default Route
