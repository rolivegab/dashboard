import PDO from '../../core/mysql-pdo'

export default class extends PDO {
	constructor() {
		super('pdo-base')
	}
	async isInMaintenance() {
		const results = await this.select('SELECT is_in_maintenance FROM server') as Array<Array<{
			is_in_maintenance: number,
		}>>

		return results[0][0].is_in_maintenance === 1
	}
}
