import Express from 'express'
import PDO from './pdo'

const Router = Express.Router()

Router.post('/', async (req, res) => {
	const DB = new PDO()
	const S = req.session as Sess
	if (!S.user) {
		return
	}
	if (req.body.command === 'general-statistics') {
		res.json({
			studentcount: await DB.getStudentCount(S.user.courses.map((i) => i.id)),
			coursesLength: S.user.courses.length,
		})
	} else if (req.body.command === 'welcome-user-block') {
		res.json({
			courses: S.user.courses,
			mainrolename: S.user.mainrolename,
			usercontextid: await DB.getUserContextId(S.user.userid),
		})
	}
})

export default Router
