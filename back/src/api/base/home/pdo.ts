import MPDO from '../../../core/mysql-pdo'

export default class extends MPDO {
	constructor() {
		super('pdo-home')
	}

	async getStudentCount(courses: number[]) {
		const result = await this.select<{studentcount: number}>(`
		SELECT COUNT(mdl_role_assignments.userid) AS studentcount
		FROM mdl_role_assignments
		LEFT JOIN mdl_context ON mdl_context.id = mdl_role_assignments.contextid
		WHERE mdl_role_assignments.roleid = 5
		AND contextlevel = '50'
		AND mdl_context.instanceid IN (?)`, [courses])

		if (result) {
			return result[0][0].studentcount
		}
	}

	async getUserContextId(userid: number) {
		const result = await this.select<{id: number}>(`
		SELECT id
		FROM mdl_context
		WHERE instanceid = ?
		AND contextlevel = 30`, [userid])

		if (result) {
			return result[0][0].id
		}
	}
}
