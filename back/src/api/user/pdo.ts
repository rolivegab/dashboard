import MPDO from '../../core/mysql-pdo'

export default class extends MPDO {
	constructor() {
		super('pdo-user')
	}

	async getMainRole(userid: number) {
		// Verifica se é professor de alguma disciplina:
		const query = `
		SELECT 1
		FROM mdl_role_assignments
		LEFT JOIN mdl_context ON mdl_context.id = mdl_role_assignments.contextid
		WHERE mdl_context.contextlevel = '50'
		AND mdl_role_assignments.userid = ?
		AND mdl_role_assignments.roleid = ?`

		let results = await this.select(query, [userid, 3]) as any as SelectConnQuery<{0: '1'}>
		if (results[0].length > 0) {
			return {id: 3, name: 'Professor'}
		}

		results = await this.select(query, [userid, 4]) as any as SelectConnQuery<{0: '1'}>
		if (results[0].length > 0) {
			return {id: 4, name: 'Tutor'}
		} else {
			return {id: 5, name: 'Estudante'}
		}
	}

	async getUseridByUsername(username: string) {
		const results = await this.select<{ id: number }>('SELECT id FROM mdl_user WHERE mdl_user.username = ?', [username])
		if (results) {
			return results[0][0].id
		}
	}
}
