import Express from 'express'
import loadUserInfo from '../../lib/load-user-info'
import userPDO from '../user/pdo'
import PDO from './pdo'

const Router = Express.Router()

Router.post('/', async (req, res) => {
	if (!req.session) {
		return
	}
	const [username, password] = [req.body.username, req.body.password]
	if (
		typeof username === 'string' &&
		typeof password === 'string'
	) {
		const DB = new PDO()
		const userDB = new userPDO()

		if (await DB.checkLogin(username, password) === true) {
			const userid = await userDB.getUseridByUsername(username)
			if (userid === undefined) {
				return
			}
			const mainUserRole = await loadUserInfo(userid, req)

			if (mainUserRole) {
				res.json({
					mainroleid: mainUserRole.id,
					success: true,
				})
			}
		} else {
			res.json({
				errorMsg: 'Usuário ou senha incorretos!',
				success: false,
			})
		}
	}
})

export default Router
