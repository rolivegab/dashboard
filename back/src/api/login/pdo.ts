import bcrypt from 'bcryptjs'
import MPDO from '../../core/mysql-pdo'

export default class extends MPDO {
	constructor() {
		super('pdo-login')
	}

	async checkLogin(username: string, password: string) {
		// Pega o usuário e a senha:
		const results = await this.select<{ username: string, password: string }>('SELECT password FROM mdl_user WHERE username = ?', [username])
		if (results && results[0].length > 0) {
			const hash = results[0][0].password.replace(/^\$2y(.+)$/i, '$2a$1')
			return await bcrypt.compare(password, hash)
		} else {
			return false
		}
	}
}
