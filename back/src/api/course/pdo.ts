import MPDO from '../../core/mysql-pdo'

export default class extends MPDO {
	constructor() {
		super('pdo-course')
	}

	async getCoursesInWhichUserTeach(userid: number) {
		const results = await this.select<{id: string, fullname: string}>(`SELECT mdl_course.id, mdl_course.fullname
		FROM mdl_role_assignments
		LEFT JOIN mdl_context ON mdl_context.id = mdl_role_assignments.contextid
		LEFT JOIN mdl_course ON mdl_course.id = mdl_context.instanceid
		WHERE
			mdl_context.contextlevel = '50'
			AND mdl_role_assignments.userid = ?
			AND mdl_role_assignments.roleid = '3'`, [userid])
		if (results) {
			return results[0]
		} else {
			return null
		}
	}
}
