import coursePDO from '../api/course/pdo'
import userPDO from '../api/user/pdo'

export default async function(userid: number, req: Express.Request) {
	const userDB = new userPDO()
	const courseDB = new coursePDO()

	const [mainUserRole, courses] = await Promise.all([userDB.getMainRole(userid), courseDB.getCoursesInWhichUserTeach(userid)])

	if (req && req.session) {
		req.session.user = {
			...{userid},
			...{userid},
			...{courses},
			mainroleid: mainUserRole.id,
			mainrolename: mainUserRole.name,
		}

		return mainUserRole
	} else {
		return
	}
}
