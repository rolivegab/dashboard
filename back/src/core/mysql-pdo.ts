import debug from 'debug'
import mysql from 'mysql2/promise'

const options = {
	charset: 'utf8',
	database: process.env.MYSQL_DATABASE,
	host: process.env.MYSQL_HOST,
	password: process.env.MYSQL_PASSWORD,
	user: process.env.MYSQL_USER,
}

const pool = mysql.createPool(options)

export default class PDO {
	protected static conn?: mysql.Connection
	protected dbg: debug.IDebugger
	constructor(dbgname: string) {
		this.dbg = debug(dbgname)
	}

	async initialize() {
		if (PDO.conn !== undefined) {
			return
		} else {
			PDO.conn = await this.getConn()
		}
	}

	async select<T = any>(query: string, values?: any) {
		return new Promise<void|T[][]>(async (resolve, reject) => {
			try {
				await this.initialize()
				if (PDO.conn) {
					const result = PDO.conn.query(query, values) as any as any[][]
					resolve(result)
				} else {
					this.dbg('Erro na conexão')
					resolve(undefined)
				}
			} catch (err) {
				this.dbg(err)
				resolve(undefined)
			}
		})
	}

	// async insert(query: string, values?: any) {
	// 	return new Promise<mysql.OkPacket[][]>(async (resolve, reject) => {
	// 		try {
	// 			const result = await this.pool.query(query, values) as any[][]
	// 			await this.pool.end()
	// 			resolve(result)
	// 		} catch (err) {
	// 			this.dbg(err)
	// 		}
	// 	})
	// }

	async getConn() {
		return await pool.getConnection()
	}
}
