import debug from 'debug'
import mongodb from 'mongodb'

export default class PDO {
	static objectID(id: string) {
		return new mongodb.ObjectID(id)
	}

	client: Promise<mongodb.MongoClient>
	db?: mongodb.Db
	dbgname: string

	constructor(dbgname: string) {
		this.dbgname = dbgname
		this.client = mongodb.MongoClient.connect(process.env.MONGO_URL as string, { useNewUrlParser: true })
	}

	async getDB() {
		const client = await this.client
		return new Promise<mongodb.Db>((resolve) => {
			if (this.db === undefined) {
				this.db = client.db(process.env.MONGO_DATABASE)
			}
			resolve(this.db)
		})
	}

	async collection(collection: string) {
		const db = await this.getDB()
		return new Promise<mongodb.Collection>((resolve, reject) => {
			try {
				resolve(db.collection(collection))
			} catch (err) {
				debug(this.dbgname)(err)
				reject(err)
			}
		})
	}

}
