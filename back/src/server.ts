// Inicializa sourcemaps:
import SourceMap from 'source-map-support'
SourceMap.install()

// Inicializa variáveis de ambiente:
import env from 'dotenv'
env.config();

import RedisStore from 'connect-redis'
import crypto from 'crypto'
import debug from 'debug'
import Express from 'express'
import Session from 'express-session'
import Redis from 'redis'
import Api from './api/router'

// Inicializa o express:
const app = Express();
const redisStore = RedisStore(Session)

// Debug:
const dbg = debug('app-server')

app
	// Inicializa a sessão, utiliza um cliente REDIS para gerenciar a sessão
	.use(Session({
		cookie: { maxAge: 2592000000 },
		resave: false,
		saveUninitialized: false,
		secret: crypto.randomBytes(8).toString('hex'),
		store: new redisStore({
			client: Redis.createClient(),
			host: process.env.REDIS_HOST as string,
			port: Number(process.env.REDIS_PORT),
		}),
	}))

	// Habilita cabeçalhos cross-origin para permitir requisições AJAX para o CLIENT_URL definido no arquivo .env
	.use((req, res, next) => {
		if (typeof process.env.CLIENT_URL === 'string') {
			res.setHeader('Access-Control-Allow-Origin', process.env.CLIENT_URL as string)
			res.setHeader('Access-Control-Allow-Credentials', 'true')
			res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
			res.setHeader('Access-Control-Allow-Headers', 'Content-Type')
			next()
		} else {
			dbg('CLIENT_URL não definido no arquivo .env')
		}
	})

	// Caso o usuário envie uma solicitação get para o endereço do back-end no caminho '/', retorna o conteúdo da sessão (DESATIVAR EM PRODUÇÃO)
	.get('/', async (req, res) => {
		if (req.session) {
			res.send({
				session: req.session,
			})
		}
	})

	// Utiliza o router 'Api' no caminho '/api'
	.use('/api', Api)

app.listen(3001, () => {
	console.log('App listen on por 3001!')
})
